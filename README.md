# Kateryna Romanovska & Pavlo Sapatsynskyi step project "Forkio"
## DAN.IT, FE#30

**Terms of reference:**  
https://gitlab.com/dan-it/groups/pe34/-/tree/master/advanced-html-css/step-project-forkio  
  
**Layout:**  
https://www.figma.com/file/9lLwBJciU4yjDZBSnqqXSS/Forkio?node-id=0%3A1  

**Technologies used:**  
* HTML5, CSS3, JS (ES6+)  
* SASS (SCSS)  
* Gulp  
* Git  
  
**Сontributors:**  
* Kateryna Romanovska (Sections: "Revolutionary", "Here is what you get", "Fork Subscription")  
* Pavlo Sapatsynskyi (Sections: "Header menu", "Hero", "People are talking about")  
  
**GitLab pages:** https://pavsgk.gitlab.io/fe-30-step-project-forkio